/**
 * MoviesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    list: function(req, res) {
        Movies.find({}).exec(function (err, movies) {
          return res.view('movies/list', {movies: movies});
        });
    },
    create: function(req, res) {
      Movies.create({
            title: req.body.title,
            description: req.body.description,
            rating: req.body.rating,
            release_date: req.body.release_date
      }).exec(function(err){
          if (err) {
              return res.send(500, {error: 'Database error'});
          }
          return res.redirect('/movies');
      });
    },
    show: async function(req, res) {
      console.log(req.params)
      var movie = await Movies.findOne({id: req.params.id}).populate('reviews');
      return res.view('movies/show', { movie: movie })
    },
    add_review: async function(req, res) {
      console.log('req body', req.body);
      var review = await Reviews.create({body: req.body.review}).fetch();
      await Movies.addToCollection(req.params.id, 'reviews').members([review.id]);
      return res.redirect('/movies/' + req.params.id);
    }
};

